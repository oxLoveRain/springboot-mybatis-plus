package com.hn.controller;

import com.hn.mapper.StudentMapper;
import com.hn.pojo.Student;
import com.hn.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("/")
public class PageController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("/find")
    public String findAll() {

        List<Student> students = studentService.queryAll();
        Iterator<Student> it = students.iterator();
        while (it.hasNext()) {
            Student obj = it.next();
            System.out.println(obj);
        }
        return "查询到学生信息";
    }

    @RequestMapping("/queryOne/{studentno}")
    public String findOne(@PathVariable("studentno") String studentno) {
        System.out.println("传入的参数是："+studentno);
        List<Student> students = studentService.selectOneStudent(studentno);
        if (students != null) {
            Iterator<Student> it = students.iterator();
            while (it.hasNext()) {
                Student obj = it.next();
                System.out.println(obj);
            }
        return "查询到一个学生对象";
        }
        return "没有查询到任何信息";
    }
}
