package com.hn.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hn.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 这里使用mybatis-plus实现大部分的sql语句
 */
@Mapper//可以在这里回国@mapper注解或者是主启动类上加入MapperScan注解
public interface StudentMapper extends BaseMapper<Student> {
    List<Student> selectOneStudent(Map<String,Object> map);
}
