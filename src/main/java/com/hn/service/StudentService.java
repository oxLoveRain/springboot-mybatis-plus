package com.hn.service;

import com.hn.mapper.StudentMapper;
import com.hn.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StudentService {

    @Autowired
    private StudentMapper studentMapper;
    //查询所有学生对象
    public List<Student> queryAll(){
        List<Student> list = studentMapper.selectList(null);
        return  list;
    };

    public List<Student> selectOneStudent(String studentno){
        Map<String, Object> map = new HashMap<>();
        map.put("studentno",studentno);
        List<Student> list = studentMapper.selectOneStudent(map);
        return list;
    }
}
