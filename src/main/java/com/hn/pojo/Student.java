package com.hn.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("student")
public class Student implements Serializable {

    private String studentno;
    private String studentname;
    private String loginpwd;
    private Character sex;
    private Integer majorid;
    private String phone;
    private String email;
    private Date borndate;

    //表示数据库没有该字段，如果不加则会抛出异常信息
    @TableField(exist = false)
    private String address;
}

